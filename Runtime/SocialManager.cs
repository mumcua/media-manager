﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static NativeShare;

public class SocialManager
{
    public void RateUs(string appStoreId = null)
    {
#if !UNITY_EDITOR && UNITY_IOS
        OpenURL("itms-apps://itunes.apple.com/app/id" + appStoreId + "?action=write-review");
#elif !UNITY_EDITOR && UNITY_ANDROID
        OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#endif
    }

    public void Share(string shareText, UnityAction<bool> _callback = null)
    {
        new NativeShare().SetText(shareText).SetCallback((result, shareTarget) =>
            {
                Debug.Log("Share result: " + result + ", selected app: " + shareTarget);
                if (_callback != null)
                {
                    if (result == ShareResult.Shared)
                    {
                        _callback.Invoke(true);
                    }
                    else
                    {
                        _callback.Invoke(false);
                    }
                }
            }).Share();
    }


    //Callback ile dönülebilir
    public  void OpenURL(string url)
    {
        Share("", (success) =>
        {
            if (success)
            {

            }
        });
        Application.OpenURL(url);
    }


}
